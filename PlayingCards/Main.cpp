#include <iostream>
#include <conio.h>
#include <string>

using namespace std;


enum Rank {
	TWO = 2,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE
};

enum Suit {
	DIAMONDS = 1,
	HEARTS,
	SPADES,
	CLUBS
};

struct CARD {
	Rank rank;
	Suit suit;
};


//The Print_Card function converts the rank and suit enumerations back into their plain english values and prints them in the form "[Rank] of [Suit]"
void Print_Card(CARD card)
{
	switch(card.rank){
	case 2: cout << "Two";
		break;
	case 3: cout << "Three";
		break;
	case 4: cout << "Four";
		break;
	case 5: cout << "Five";
		break;
	case 6: cout << "Six";
		break;
	case 7: cout << "Seven";
		break;
	case 8: cout << "Eight";
		break;
	case 9: cout << "Nine";
		break;
	case 10: cout << "Ten";
		break;
	case 11: cout << "Jack";
		break;
	case 12: cout << "Queen";
		break;
	case 13: cout << "King";
		break;
	case 14: cout << "Ace";
		break;
		}
	cout << " of ";
	switch (card.suit) {
	case 1: cout << "Diamonds";
		break;
	case 2: cout << "Hearts";
		break;
	case 3: cout << "Spades";
		break;
	case 4: cout << "Clubs";
		break;
	}


}

//The High_Card function compares the card ranks and returns whichever is higher. If they are equal it compares the card suits. If the cards are identical, it returns card1.
CARD High_Card(CARD card1, CARD card2) {
	if (card1.rank > card2.rank)
	{
		return card1;
	}
	if (card1.rank < card2.rank)
	{
		return card2;
	}
	if(card1.suit > card2.suit)
	{
		return card1;
	}
	if (card1.suit < card2.suit)
	{
		return card2;
	}
	else return card1;
}

//The Low_Card function compares the card ranks and returns whichever is lower. If they are equal it compares the card suits. If the cards are identical, it returns card1.
CARD Low_Card(CARD card1, CARD card2) {
	if (card1.rank < card2.rank)
	{
		return card1;
	}
	if (card1.rank < card2.rank)
	{
		return card2;
	}
	if (card1.suit < card2.suit)
	{
		return card1;
	}
	if (card1.suit < card2.suit)
	{
		return card2;
	}
	else return card1;
}

int main() {
	//This loop will keep going until the user inputs N, indicating they want to stop playing. The user will input a card rank and suit for card1, then a card rank and suit will randomly
	//be assigned to card2. Then, it is randomly decided whether high or low card wins and the winner is declared.
	while (true) {
		int rank_input_1;
		int suit_input_1;
		int rank_input_2 = rand() % 12 + 2;
		int suit_input_2 = rand() % 3 + 1;

		CARD card1;
		CARD card2;

		cout << "Please enter a card rank (2-14): ";
		cin >> rank_input_1;
		cout << "Please enter a card suit (1:Diamonds 2:Hearts 3:Spades 4:Clubs): ";
		cin >> suit_input_1;

		card1.rank = (Rank)rank_input_1;
		card1.suit = (Suit)suit_input_1;
		card2.rank = (Rank)rank_input_2;
		card2.suit = (Suit)suit_input_2;

		cout << "You chose the ";
		Print_Card(card1);
		cout << "! \n";

		cout << "The computer randomly chose the ";
		Print_Card(card2);
		cout << "!\nPress any key to see who won!\n\n";
		_getch();

		if ((rand() % 2) > 0) {
			cout << "The probability mages have decided that the high card wins.\n\n";
			cout << "The winner is the ";
			Print_Card(High_Card(card1, card2));
			cout << "!";
		}
		else {
			cout << "The probability mages have decided that the low card wins.\n\n";
			cout << "The winner is the ";
			Print_Card(Low_Card(card1, card2));
			cout << "!\n\n";

			char continue_input;
			cout << "Play again? (Y/N)";
			cin >> continue_input;
			cout << "\n";

			if (continue_input == 'N' || continue_input == 'n') {
				return 0;
				}
		}
	}
}